#ifndef __NODE_H__
#define __NODE_H__

/*this library corresponds to the fourth level*/

#include "lowpan.h"
//#include "uart/handleMsg.h"

#define ROOT_ELECTION_DISPATCH 0x44 //010001xx
#define VOTE_DISPATCH 0x48          //010010xx
#define GATEWAY_DISPATCH 0x4C       //010011xx
#define PRESENT_DISPATCH 0x50       //010100xx
#define REQUEST_UPLINK 0x54         //010101xx
#define UPLINK_REPLY 0x58           //010110xx
#define HEARTBEAT 0x5C              //010111xx
#define HELLO_NODE 0x64             //011001xx
#define HELLO_REPLY 0x68            //011010xx
#define SEND_GW_COMPLETE 0x6C       //011011xx
#define REQUEST_REPLY 0x70          //011100xx
#define ELECTION_COMPLETE 0x74      //011101xx
#define CHECK_ROOT 0x7C             //011111xx

#define LISTMESSAGE 0x02
         
typedef struct
{
    /*message for tx uart*/
    uint8_t * rx_msg;
    uint16_t len;
    uint16_t srcAddr; 

}rxMessage_t;

typedef struct
{
    uint8_t type;
    uint16_t len;
    uint8_t * buf;   
}serial_t;

enum type {
    idle = 0,
    lead = 1,
    middle = 2,
    leaf = 3,
    gateway = 4
};

typedef struct
{
    uint16_t address;
    float rssi;
    unsigned long uptime;
    uint8_t role;
    uint8_t current_level;
    uint8_t ndownlink;
}parent_t;



typedef struct
{
    uint16_t address;
    float RSSI;
    unsigned long uptime;
    uint8_t num_votes;
    bool started = false;
}voter_t;




struct node_lessThan
{
    bool operator()(parent_t const & a, parent_t const & b) const
    {
        if(a.current_level < b.current_level)
        	return true;
        else if(a.current_level == b.current_level)
        {
        	if (a.ndownlink == b.ndownlink)
        	{
        		if(a.rssi == b.rssi)
        			return a.uptime < b.uptime;
        		else
        			return a.rssi > b.rssi;
        	}
            else
        	    return a.ndownlink < b.ndownlink;
        }

        return false;        
    }

    bool operator()(voter_t const &a, voter_t const &b) const
    {
        if(a.RSSI > b.RSSI)
            return true;
        else if(a.RSSI == b.RSSI)
        {
            if(a.uptime < b.uptime)
                return true;
        }

        return false; 
    }

};


class NODE
{
    public:
        NODE();
        
        ~NODE();

        void init(int panid,int channel,int gw);
        
        void NetInterrupt();

        void Send(uint8_t * msg, uint16_t len);

        
    protected:

        
        /*----- for application layer only -----*/

        rxMessage_t rx_message; 

        /*---------------------------------------*/
        bool Receive(uint8_t (*AppSensor)(void));
        //void handleRequestMessage(uint8_t * data, uint16_t& len);
        
        float th;
        int iteration;

        uint16_t panid;
        uint16_t address;
        
        uint16_t addrGateway = 0;

        Lowpan * net;

        void SelfOrganization(uint8_t (*AppSensor)(void));
        
        void setTimer();

        /*start the election root node*/
        void setElection(bool option);
         /*populate candidate list parents node*/
        void populateMiddleNodeCandidate(float const &, uint8_t*, uint8_t const &, uint16_t const &, bool frameON);
        /*send the presentation frame*/
        void sendPresentationFrame();
        /*send the rssi between itself node and gateway*/
        void SendInformationGatewayNode(bool);
        /*send vote for root node*/
        void SendVoteRootNode();
        /*receive and save node informations in the list*/
        void PopulateInfoRootNodes(uint8_t*,uint16_t const &, uint16_t const &len, bool &);
        /*return the attendible middle nodes*/
        vector <parent_t> getAttendibleParents();
        /*return the node's role*/
        int getRole();
        /*set node role*/
        void setRole(int role);
        /*set the the threshold RSSI*/
        void setThreshold(float threshold);
        /*get the threshold*/
        float getThreshold();
        /*gateway send RSSI*/
        uint8_t * sendGatewayRssi();
        /*receive RSSI from the gateway*/
        void getGatewayRSSI(uint8_t*,uint8_t const &, uint16_t const &);
        /*start gateway task*/
        void startRssiGatewayTask();

        uint16_t getParentNode();
        void setParentNode(uint16_t);

        void SendToPan(uint8_t * message, uint16_t const &dim, uint16_t const &addr, bool heapfree);

        void PrintVoters();
        
        bool cleanTimeoutRoutingTable();

        uint8_t getCurrentMeshLevels();
        
        void SendRoutingTableToParantNode();

        
        
        vector <parent_t> candidate;
        vector <voter_t> voters;
        
        float rssiGateway = 0; //dbm
        bool rootFound = false;
        uint8_t cnt_fault_node = 0;

        bool startElection = false;

        bool resetTime = true;

        uint32_t timerElection = 0;
        
        int role;

        uint8_t * GetRoutingTable();
        int GetTableSize();
        void UpdateRoutingTable(uint8_t * packet,uint16_t const &addr, uint16_t const &len = 0);
        void AddRecordTable(uint16_t const &);

        
       
        uint8_t nlinkParentNode = 0;
        uint8_t levelParentNode = 0;
        float rssiParentNode = 0;

        uint32_t tm_presentation = 0;
        uint32_t tm_checkFaultLeaf = 0;

        uint16_t rootAddr = 0;

        
        
        void check_events();

        void SendPairRequest();
        void ReceiveRequest(uint8_t * msg, uint16_t const &addr);
        void checkFaultNode();
        void electionRoot(int const &);

        void sendPacketProva();
        void ReceivePacketProva(uint8_t *);

    
        void onReceive(void (Lowpan::*recv)(void));

        void checkGatewayRequest(uint8_t * msg, uint16_t len);
        
};

#endif