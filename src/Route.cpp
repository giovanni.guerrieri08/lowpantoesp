#include "Route.h"

Route::Route() {}

Route::~Route() {}

uint16_t Route::getNextHopAddress(uint16_t const &addr)
{
    auto el = find_if(table.begin(),table.end(),[addr](const vector<uint16_t> &vet) \
    {return (addr == vet[0]);});
    
    if(el != table.end())
    {
        return (*el)[1];
    }

    return NULL;
}

uint16_t Route::getMaxHops()
{
    auto MaxHop = max_element(table.begin(),table.end(), \
    [] (const vector<uint16_t> &v1, const vector<uint16_t> &v2) {return (v1[2] < v2[2]);});

    if(MaxHop != table.end())
        return (*MaxHop)[2];
    return 0;
}

bool Route::checkChild(uint16_t const &addr)
{
    auto it = find_if(table.begin(), table.end(), [addr] \
                (const vector<uint16_t>& vet) {return (vet[0] == addr);});

    if(it != table.end())
        return true;
    return false;
}

bool Route::checkDirectChild(uint16_t const &addr)
{
    auto it = find_if(table.begin(), table.end(), [addr] \
                (const vector<uint16_t>& vet) {return (vet[0] == addr && vet[1] == addr);});

    if(it != table.end())
        return true;
    return false;
}

void Route::resetLifeTime(uint16_t const &addr)
{
    auto it = find_if(table.begin(), table.end(), [addr] \
                (const vector<uint16_t>& vet) {return (vet[0] == addr && vet[1] == addr);});

    if(it != table.end())
    {
        (*it)[3] = 100;
    }
}

int Route::checkLifeTimeRecord()
{
    int count = 0;
    for (auto it = table.begin(); it != table.end();)
    {
        if((*it)[0] == (*it)[1])
        {
            (*it)[3]--;

            if((*it)[3] == 0)
            {
                uint16_t addr = (*it)[0];

                while( ((it = table.erase(it)) != table.end()) && ((*it)[1] == addr) );

                count++;
            }
            else
                it++;

        }
        else
            it++;
    }
    
    return count;
}

uint16_t Route::getAddrParentNode()
{
    return AddrParentNode;
}

void Route::setParentNode(uint16_t addr)
{
    this->AddrParentNode = addr;
}

void Route::AddRecord(uint16_t const &addrDest, uint16_t const &nextHop, uint16_t const &hop)
{
   
    vector<uint16_t>row{addrDest,nextHop,hop};

    if(addrDest == nextHop)
        row.push_back(100);

    table.push_back(row);

    row.clear();
}

void Route::DelRecordAll(uint16_t const &addr)
{
    for (auto it = table.begin(); it != table.end();)
    {
        if((*it)[1] == addr)
            it = table.erase(it);
        else
            it++;
    }    
}

void Route::DelRecord(uint16_t const &addr)
{
    
    auto el = find_if(table.begin(), table.end(), [addr] \
            (const vector<uint16_t>& vet) {return (vet[0]==addr);});
    
    if(el != table.end())
        table.erase(el);

}

void Route::Update(uint8_t * packet,uint16_t const &addr, uint16_t const &len)
{
    if(*packet == ROUTING_UPDATE)
    {
        record_t * row = (record_t*)malloc(sizeof(record_t));
        
        uint16_t i = 1;
        
        //auto el = find_if(table.begin(), table.end(), [addr] \
            (const vector<uint16_t>& vet) {return (vet[1]==addr);});

        //if(el != table.end())
        //{
            vector <uint16_t> tmp;
            
            while(i<len)
            {    
                memcpy(row,(record_t*)&packet[i],sizeof(record_t));
                
                uint16_t destAddr = row->dest_addr;
                
                auto it = find_if(table.begin(), table.end(), [destAddr] \
                (const vector<uint16_t>& vet) {return (vet[0] == destAddr);});

                if(it != table.end())
                {
                    (*it)[2] = (row->hop)+1;
                    (*it)[1] = addr;
                }
                    
                else
                    AddRecord(destAddr,addr,(row->hop)+1);
                
                /*put in the tmp array all the address of the child node that have sent the her routing table*/
                tmp.push_back(destAddr);

                i+=sizeof(record_t);
            }
            
            
            for (auto it = table.begin(); it != table.end();) 
            { 
                if((*it)[1] == addr)
                {
                    auto it_tmp = find(tmp.begin(), tmp.end(), (*it)[0]);
                    
                    if(it_tmp == tmp.end())
                        it = table.erase(it);
                    else
                        it++;    
                }
            }
            
            tmp.clear();
            vector<uint16_t>().swap(tmp);

            AddRecord(addr,addr,1);

            sort(table.begin(),table.end(), [](const std::vector<uint16_t> &p, \
            const std::vector<uint16_t> &m)
            { 
                return (p[1] == m[1]);	
	        });


	        reverse(table.begin(),table.end());

            
        //}
        /*else
        {
            //AddRecord(addr,addr,1);

            while(i<len)
            {
                memcpy(row,(record_t*)&packet[i],sizeof(record_t));

                AddRecord(row->dest_addr,addr,(row->hop)+1);

                i+=sizeof(record_t);
            }
        }*/


        free(row);
    }
    else if(*packet == ROUTING_DELETE)
    {
        DelRecordAll(addr);
    }
}

uint8_t * Route::PrepareRoutingPacket()
{
    if(table.size()== 0)
        return NULL;
    
    uint16_t i = 0;
    uint8_t * packet = (uint8_t*)calloc(GetPacketSize(),sizeof(uint8_t));

    packet[i++] = ROUTING_UPDATE;

    record_t rec;

    for (auto it = table.begin(); it != table.end(); it++) 
    { 
        rec.dest_addr = (*it)[0];
        rec.hop = (*it)[2];

        memcpy(&packet[i],(uint8_t*)&rec,sizeof(record_t));
        i+=sizeof(record_t);
    }

    return packet;
}

uint16_t Route::GetPacketSize()
{
    return 1+(table.size()*sizeof(record_t));
}

uint16_t Route::GetTableSize()
{
    return table.size();
}

void Route::PrintRoutingTable()
{
    printf("Routing table:\n");

    for (auto it = table.begin(); it != table.end(); it++) 
    { 
        if((*it)[0] == (*it)[1])
            Serial.printf("%x\t%x\t%x\t%d\n",(*it)[0],(*it)[1],(*it)[2],(*it)[3]);
        else
            Serial.printf("%x\t%x\t%x\n",(*it)[0],(*it)[1],(*it)[2]);
    }
    
}

