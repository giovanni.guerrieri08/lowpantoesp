#ifndef PAN_DEF_H
#define PAN_DEF_H

#include "Arduino.h"

#define DISPATCH_FRAG1 0xC0
#define DISPATCH_FRAGN 0xE0

typedef union 
{

	struct base
	{
		uint8_t payload[109];
	}normal;

	struct first_frag
	{
	    uint8_t numFragment;   
	    uint8_t payload[108];
	    
	}firstfrag;    
    

    struct next_frag
	{
	    uint16_t offset;
	    uint8_t numFragment;
	    uint8_t payload[105];
	    
	}nextfrag;

}body_t;


typedef struct
{
	uint16_t srcAddr;
	uint16_t destAddr;

	uint8_t fragment;

	uint8_t lenMessage;


	 /*	0: no fragment
        0xC0: first fragment
        0xE0: more or last fragment  */

	body_t body;
	

}msg_t;


typedef struct rx_msg
{
	msg_t message;
	uint8_t rssi;
	uint8_t lqi;

}rx_msg_t;



#endif