#include "node.h"
#include <Arduino.h>

/*-------------------configuration mcu-------------*/

bool volatile timerFires = false;

#ifdef ESP32

#include "WiFi.h"

hw_timer_t * timer = NULL;
portMUX_TYPE MuxTimer = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer()
{
   portENTER_CRITICAL_ISR(&MuxTimer);

   timerFires = true;

   portEXIT_CRITICAL_ISR(&MuxTimer);
}

void startTimer()
{
  
  /*clock frequency: 80Mhz,
  prescale value (16bit value): 40000
  true: counts up  
  */  
  timer = timerBegin(0,40000,true);  
  timerAttachInterrupt(timer,&onTimer,true);
  //2000: value for trigger interrupt handler "ontimer"
  timerAlarmWrite(timer,2000,true); /*every second*/
  
  timerAlarmEnable(timer);

}

#endif

#ifdef ESP8266

#include <ESP8266WiFi.h>
#include <Ticker.h>

Ticker timer;


void ICACHE_RAM_ATTR onTimerISR()
{
  
   timerFires = true;   
}

void startTimer()
{
   timer1_attachInterrupt(onTimerISR);
   timer1_enable(TIM_DIV256,TIM_EDGE,TIM_LOOP);
   timer1_write(312500); //every one second
}

#endif


/*-----------end mcu configuration----------------*/


bool FramePresentation = true;
bool PairConfirmed = false;
bool PairWait = false;


uint32_t timeStartWait = 0;

uint32_t timePresentation = 0;
uint32_t timeLapsePresent = millis();

uint32_t timeRequest = 0;
uint32_t timeCheckPresent = 0;
uint32_t HeartBeatCount = 0;

bool startedNet = false;

uint32_t timePred = millis();
uint32_t TimeProva = millis();


uint16_t lenDataMessage = 0;

/*for lead election*/

bool SendGw = false;
int numSendGw = 0;
bool startVotes = false;
uint16_t prevAddress = 0;
bool SendVote = false;
bool Sent = false;
uint16_t nextVoterAddr = 0;
int countIteration = 0;

/*----------------*/

#ifdef ESP32
void InterruptTask( void * arg);
SemaphoreHandle_t xSemaphore = NULL;
#endif



void NODE::Send(uint8_t * msg, uint16_t len)
{
    if(rootFound)
    {
        SendToPan(msg,len,rootAddr,false);
    } 
        
    
    
}

void NODE::onReceive(void (Lowpan::*recv)(void))
{
    (net->*recv)();
}
    
NODE::NODE() {} 

NODE::~NODE() {}


void NODE::setParentNode(uint16_t addr)
{
    net->getRoutingTable()->setParentNode(addr);
}

uint16_t NODE::getParentNode()
{
    return net->getRoutingTable()->getAddrParentNode();
}

void NODE::init(int panid, int channel,int gw)
{
    srand((unsigned int)WiFi.macAddress().toInt());
    uint16_t addr = (((uint16_t)rand()) % 65534) + 1;

    if(!gw)
        role = idle;
    else
    {
        role = gateway;
    }

    this->address = addr;
    this->panid = panid;

    net = new Lowpan();
    net->init(panid,channel,address);

    
    #ifdef ESP32
    xSemaphore = xSemaphoreCreateBinary();
    xTaskCreatePinnedToCore(InterruptTask,"interrupt",2048,(void*)net,4,NULL,1);
    
    //printf("MRF24J40 Addr of ESP32: %x\n", addr );

    #endif

    #ifdef ESP8266

    printf("MRF24J40 Addr of ESP8266: %x\n", addr );

    #endif

    timeStartWait = 0; 

    //gwMessage = new handleMsg();

    startTimer();

}

void NODE::SendToPan(uint8_t* buf,uint16_t const &dim, uint16_t const &addr, bool heapfree)
{
    check_events();
    
    net->Send(buf,dim,addr,heapfree);
}

bool NODE::cleanTimeoutRoutingTable()
{
    int nodiDelete = net->getRoutingTable()->checkLifeTimeRecord();
    net->currentDwlink -= nodiDelete;

    if(nodiDelete > 0)
        return true;
    
    return false;

}
void NODE::populateMiddleNodeCandidate(float const &threshold, uint8_t * bufRecv, uint8_t const &RSSI, uint16_t const &srcAddr, \
bool frameON)
{
    if((*bufRecv & 0xFC) == PRESENT_DISPATCH)
    {
        
        if((srcAddr == getParentNode()) && (role != lead) && (role != gateway))
        {
            levelParentNode = (bufRecv[1] & 0xF0) >> 4;
            nlinkParentNode = bufRecv[1] & 0x0F;
            cnt_fault_node = 0;
            rssiParentNode = ((80*RSSI)/255.0)-100;
            net->currentLayer = levelParentNode+1;
        }
        else if((role != lead) && (role!=gateway) && (role!=leaf) && (frameON))
        {
            rootFound = true;

            if(!(net->getRoutingTable()->checkChild(srcAddr))) //evita i loopback
            {
                Serial.printf("\npresentation frame received!\n");

                //float rssi = (((0.2*RSSI)-88)+100)/80;
                float rssi = ((80*RSSI)/255.0)-100;

                float RssiPercent = ((rssi+100)/80.0)*0.99;

                if(RssiPercent > threshold)
                {
                    int i = 0;
                    
                    parent_t parent;

                    if(bufRecv[i] & 0x01)
                        parent.role = middle;
                    else
                        parent.role = lead;

                    /*current level node*/    
                    parent.current_level = (bufRecv[++i] & 0xF0) >> 4;
                    
                    /*current downlink*/
                    parent.ndownlink = bufRecv[i++] & 0x0F;
                
                    /*address of the parent node*/
                    parent.address = srcAddr;
                    
                    /*uptime node*/ 
                    memcpy(&parent.uptime,(unsigned long*)&bufRecv[i],sizeof(unsigned long));
                    
                    /*rssi %*/    
                    parent.rssi = rssi;

                    auto el = find_if(candidate.begin(), candidate.end(),\
                    [parent](const parent_t &it) {return (parent.address == it.address);});
                    
                    /*insert the attendible parent in the list and sort element*/
                    
                    if (!(el != candidate.end()))
                    {
                        candidate.push_back(parent);
                    }   
                        
                    else
                    {
                        el->current_level = parent.current_level;
                        el->ndownlink = parent.ndownlink;
                        el->uptime = parent.uptime;
                        el->role = parent.role;
                        el->rssi = parent.rssi; 
                    }
                    
                    sort(candidate.begin(), candidate.end(), node_lessThan());
                    // print out content:
                    for (auto it =candidate.begin(); it!=candidate.end(); ++it){
                        Serial.printf("address: %x\n", it->address);
                        Serial.printf("level mesh: %d\n", it->current_level);
                        Serial.printf("n_downlink: %d\n", it->ndownlink);
                        Serial.printf("rssi: %f\n", it->rssi);
                        Serial.printf("uptime: %lu\n", it->uptime);    
                    }
                        
                    Serial.println();

                }
                //printf("rssi value: %2.0f dbm\n", (0.2*rxPacket.rssi)-88);
                //printf("rssi percentage: %1.2f\n", (rssi+100)/80);
            }
            
        }
    }
}


void NODE::sendPresentationFrame()
{
    if(net->currentDwlink < net->MaxDwlink)
    {
        int i = 0;

        uint8_t buf[10];
        memset(buf,0,sizeof(buf));

        buf[i] = PRESENT_DISPATCH; //dispatch value     01110111|10010110
        
        if(role == middle)
            buf[i] |= 1; //middle or lead node
        
        buf[++i] |= net->currentLayer << 4; //current level
        buf[i++] |= net->currentDwlink; //number downlink
        
        unsigned long time = millis()/1000; //uptime in seconds 
        Serial.printf("uptime before send packet: %lu\n", time);

        memcpy(&buf[i],(uint8_t*)&time,sizeof(unsigned long));
        
        Serial.printf("package sent!\n");
        
        for (int j = 0; j < sizeof(unsigned long)+i; j++)
        {
            Serial.printf("%x",buf[j]);
        }
        
        Serial.printf("\n");
            
        SendToPan(buf,10,BROADCAST_ADDRESS,false);
    }

}


void NODE::electionRoot(int const &iteration)
{
    
    if(rssiGateway == 0)
        return;

    if(countIteration < iteration)
    {
        if(!startVotes)
        {
            if(!SendGw)
            {   
                if(numSendGw < 5)
                {
                    if(timerElection > 1 )
                    {
                        SendInformationGatewayNode(false);
                        numSendGw++;
                        timerElection = 0;
                    }
                }
                else{
                    SendGw = true;
                    numSendGw = 0;
                    Serial.println("ho finito di ispezionare, avviso gli altri!");
                }    
            }
            else
            {
                if(numSendGw < 5)
                {
                    if(timerElection > 1 )
                    {
                        SendInformationGatewayNode(true);
                        numSendGw++;
                        timerElection = 0;
                    }
                }

                else
                {   
                    /*controllo che tutti hanno finito l'ispezione*/
                    auto allFinished = find_if(voters.begin(),voters.end(), [](const voter_t &e)\
                    {return (e.started == false);});

                    if(allFinished == voters.end()) 
                    {
                        Serial.println("Sono nella seconda fase!");
                        startVotes = true;
                        
                        if(address == voters.begin()->address)
                        {
                            Serial.println("inizio io!");
                            prevAddress = voters.begin()->address;
                            SendVote = true;
                            timerElection = 0;
                        }
                    }
                        
                    else
                    {
                        if(timerElection > 2)
                        {
                            Serial.printf("il nodo %x non ha finito di esplorare!\n",allFinished->address);
                            uint8_t msg = SEND_GW_COMPLETE;
                            SendToPan((uint8_t*)&msg,1,allFinished->address,false);
                            timerElection = 0;
                        }     
                    }
                }
            }
        }
        else
        {
            /*qui si inizia a votare*/
            
            if((prevAddress == address) && (Sent) && (!SendVote))
            {
                if(timerElection > 10)
                {
                    Serial.println("Rimando il pacchetto!");
                    SendVote = true;
                    timerElection = 0;   
                }
            }

            SendVoteRootNode();                
        }
    }
    else
    {
        if(startVotes)
        {
            timerElection = 0;
            startVotes = false;
        }
        /*Elezione conclusa*/

        if(timerElection > 1)
        {
            /*ordino la lista in base al numero di voti ricevuti*/
            sort(voters.begin(),voters.end(),[](const voter_t &a, const voter_t &b)\
            {return (a.num_votes > b.num_votes);});
            
            if(voters.begin()->address == address)
            {
                role = lead;
                net->currentLayer = 0;
                net->currentDwlink = 0;
               
                setParentNode(addrGateway);
                uint8_t msg = ELECTION_COMPLETE;
                delay(20);
                SendToPan((uint8_t*)&msg,1,addrGateway,false); 
            }
            
            rootFound = true;
            startElection = false;
            voters.clear();
            timerElection = 0;
            SendGw = false;
            numSendGw = 0;
            prevAddress = 0;
            nextVoterAddr = 0;
            SendVote = false;
            Sent = false;
            countIteration = 0;
            timerElection = 0;
        }       
    }
}

void NODE::SendInformationGatewayNode(bool finish)
{

    if(!finish)
    {    
        uint16_t len = 1+sizeof(voter_t);
        uint8_t * buf = (uint8_t*)calloc(len,sizeof(uint8_t));

        buf[0] = ROOT_ELECTION_DISPATCH;
        Serial.println("Ispeziono i votanti!");
        uint16_t thisAddress = address;
        
        auto me = find_if(voters.begin(),voters.end(), [thisAddress](const voter_t &e){\
        return (e.address == thisAddress);});

        if(me != voters.end())
        {
            memcpy(&buf[1],(uint8_t*)&*me,sizeof(voter_t));
        }
        else
        {
            voter_t me;
            me.address = address;
            me.num_votes = 0;
            me.RSSI = rssiGateway;
            me.uptime = millis()/1000;

            voters.push_back(me);

            memcpy(&buf[1],(uint8_t*)&me,sizeof(voter_t));
        }

        SendToPan(buf,len,BROADCAST_ADDRESS,true);
    }
    else
    {
        uint8_t message = ROOT_ELECTION_DISPATCH | 1;
        
        uint16_t myaddr = address;

        auto me = find_if(voters.begin(),voters.end(),[myaddr](const voter_t &it)\
        { return myaddr == it.address;});
        
        if(me != voters.end())
        {
            if(!me->started)
            {
                
                me->started = true;
            }
            
            SendToPan((uint8_t*)&message,1,BROADCAST_ADDRESS,false);    
        }  
    }
}

void NODE::SendVoteRootNode()
{
    
    if( ((prevAddress == address) && (SendVote)) || (voters.size() == 1) )
    {
        SendVote = false;
        Serial.printf("E' il mio turno! %x e siamo alla %d iterazione\n",address,countIteration);
        
        uint16_t len = 4+sizeof(voter_t);
        uint8_t * buf = (uint8_t*)calloc(len,sizeof(uint8_t));

        buf[0] = VOTE_DISPATCH;
        
        if(!Sent)
        {
            if(voters.size() != 1)
                Sent = true;

            voters.begin()->num_votes++;

            auto el = find_if(voters.begin(),voters.end(),[](const voter_t &e)\
            {return (e.address == prevAddress);});

            if(++el == voters.end())
            {
                countIteration++;

                Serial.printf("iterazione aumentata a %d\n",countIteration);
                
                nextVoterAddr = voters.begin()->address;
            }
            else
            {
                nextVoterAddr = el->address;
            }

        }
        
        buf[1] = countIteration;
        buf[2] = nextVoterAddr >> 8;
        buf[3] = nextVoterAddr;
            
        memcpy(&buf[4],(uint8_t*)&*(voters.begin()),sizeof(voter_t));
            
        SendToPan(buf,len,BROADCAST_ADDRESS,true);

    }
}


void NODE::PrintVoters()
{
    if(!startVotes)
    {
        for (auto i = voters.begin(); i != voters.end(); i++)
        {
            Serial.printf("addr: %x\n",i->address);
        }

        if(!voters.empty())
            Serial.println("-----------------------------");
    }
}


/*first phase*/
void NODE::PopulateInfoRootNodes(uint8_t * bufRecv, uint16_t const &addr, uint16_t const &len, bool &netState)
{
    
    if(*bufRecv == ELECTION_COMPLETE)
    {
        startElection = false;
        rootFound = true;
        voters.clear();
    }

    if((*bufRecv == ROOT_ELECTION_DISPATCH) || (*bufRecv == VOTE_DISPATCH))
    {

        if((rootFound) && (levelParentNode == 0) && (role!=gateway))
        {        
            rootFound = false;
            cnt_fault_node = 0;
            startElection = true;
            setParentNode(0);
            role = idle;
        }
    }    
    
    if((role == idle) && !(rootFound))
    {
        voter_t node;

        if((*bufRecv & 0xFC) == ROOT_ELECTION_DISPATCH)
        {
            if(!(*bufRecv & 0x03))
            {
                
                if(!startElection)
                    startElection = true;
                
                memcpy(&node,(voter_t*)&bufRecv[1],sizeof(voter_t));

                
                auto el = find_if(voters.begin(),voters.end(),[node](const voter_t &it)\
                { return node.address == it.address;});

                if(el == voters.end())
                {
                    voters.push_back(node);
                }
                else
                {
                    el->uptime = node.uptime;
                    el->num_votes = node.num_votes;
                    
                }
                    
                sort(voters.begin(), voters.end(), node_lessThan());
                
            }
            else
            {
                auto el = find_if(voters.begin(),voters.end(),[addr](const voter_t &it)\
                { return addr == it.address;});

                if(el != voters.end())
                {
                    if(!el->started)
                    {
                        el->started = true;
                        Serial.printf("%x ha finito\n",el->address);
                    }
                }

            }
        }
        else if( (*bufRecv & 0xfc) == REQUEST_REPLY )
        {
            Serial.println("HO RICEVUTO UNA REQUEST REPLY!");
            if(*bufRecv & 0x03)
            {
                auto el = find_if(voters.begin(),voters.end(),[addr](const voter_t &it)\
                { return addr == it.address;});

                if(el != voters.end())
                {
                    el->started = true;
                    Serial.printf("%x ha finito\n",el->address);
                }

            }
        }

        else if(*bufRecv == VOTE_DISPATCH) 
        {            
            if(prevAddress != addr)
            {
                prevAddress = addr;
    
                countIteration = bufRecv[1];
                Serial.printf("ho ricevuto l'iterazione: %d\n",countIteration);
                
                nextVoterAddr = bufRecv[2] << 8;
                nextVoterAddr |= bufRecv[3];

            
                memcpy(&node,(voter_t*)&bufRecv[4],sizeof(voter_t));
                
                auto el = find_if(voters.begin(),voters.end(),[node](const voter_t &e)\
                {return (node.address == e.address);});

                if(el != voters.end())
                {
                    el->num_votes = node.num_votes;
                }
                else
                {
                    voters.push_back(node);
                }
                    
                sort(voters.begin(), voters.end(), node_lessThan());

                
                if(nextVoterAddr == address)
                {
                    SendVote = true;
                    prevAddress = nextVoterAddr;                              
                    Sent = false;

                }   
                
        
                if(voters.size() > 0)
                {
                    // print out content:
                    Serial.printf("lista voti:\n");
                    for (vector<voter_t>::iterator it=voters.begin(); it!=voters.end(); ++it)
                    {  
                        Serial.printf("address: %x\n", it->address);
                        Serial.printf("numeri voti: %d\n", it->num_votes);    
                    }

                    Serial.printf("----------------------------------------\n");
                }
            }      
        }
    }
}


vector <parent_t> NODE::getAttendibleParents()
{
    return candidate;
}

int NODE::getRole()
{
    return role;
}

void NODE::setThreshold(float threshold)
{
    th = threshold;
}

float NODE::getThreshold()
{
    return th;
}

uint8_t * NODE::sendGatewayRssi()
{
    
    uint8_t * buf = (uint8_t*)calloc(1,sizeof(uint8_t));
    buf[0] = GATEWAY_DISPATCH;

    return buf;
}

void NODE::getGatewayRSSI(uint8_t * packet, uint8_t const &rssi, uint16_t const &addr)
{
    
    if(*packet == GATEWAY_DISPATCH)
    {
        if(rssiGateway == 0)
        {
            Serial.printf("sono qua nel getGateway\n");
            
            rssiGateway = ((80*rssi)/255.0)-100;
            addrGateway = addr;
            //startElection = true;
        }
    }       
    
}


void NODE::setRole(int role)
{
    this->role = role;
}


void NODE::UpdateRoutingTable(uint8_t * packet,uint16_t const &addr, uint16_t const &len)
{
    net->getRoutingTable()->Update(packet,addr,len);
}

void NODE::AddRecordTable(uint16_t const &addr)
{
    net->getRoutingTable()->AddRecord(addr,addr,1);
}

void NODE::SendRoutingTableToParantNode()
{
    SendToPan(net->getRoutingTable()->PrepareRoutingPacket(),net->getRoutingTable()->GetPacketSize(),getParentNode(),true);

}

uint8_t NODE::getCurrentMeshLevels()
{
    return net->currentLayer+(net->getRoutingTable()->getMaxHops());
}


void NODE::check_events()
{
    if(net->getMac()->getFlagRx()) 
    {
        net->getMac()->resetRx();

        if(!(net->getMac()->get_rxinfo()->typeFrame)) /*dataframe*/
        {  
            onReceive(&Lowpan::onReceive);
            
        }    
        else
        {
            if(role != gateway)
            {

                Serial.println("-------------------------");
                Serial.println("ACK FRAME:");
                Serial.printf("fcf: %x\n",net->getMac()->get_rxinfo()->frame.AckFrame.fcf.framecontrol);
                Serial.printf("num seq: %d\n",net->getMac()->get_rxinfo()->frame.AckFrame.num_seq);
                Serial.printf("fcs: %x\n",net->getMac()->get_rxinfo()->frame.AckFrame.fcs);
                Serial.println("-------------------------");
            }
        }
    }
    if (net->getMac()->getFlagTx())
    {
        net->getMac()->resetTx();
        
        //net->getMac()->handle_tx();
    }
}


bool NODE::Receive(uint8_t (*AppSensor)(void))
{
    SelfOrganization(AppSensor);

    while(!(net->fifo.empty()))
    {
        rx_msg_t * Msg = net->fifo.front();
        net->fifo.pop_front();

        uint8_t * message = net->MessageProcess(Msg);

        if(message != NULL)
        {
            if((message[0] & 0xc0) == 0x40)
            {
                
                /* service message network  */

                populateMiddleNodeCandidate(0.5,message,Msg->rssi,Msg->message.srcAddr, FramePresentation);
                
                PopulateInfoRootNodes(message,Msg->message.srcAddr,Msg->message.lenMessage,startedNet);
                
                getGatewayRSSI(message,Msg->rssi,Msg->message.srcAddr);
                UpdateRoutingTable(message,Msg->message.srcAddr,Msg->message.lenMessage);
                
                ReceiveRequest(message,Msg->message.srcAddr);

                PrintVoters();

                free(message);
                free(Msg);
                
            } 
            else
            {

                rx_message.rx_msg = message;
                rx_message.len = Msg->message.lenMessage;
                rx_message.srcAddr = Msg->message.srcAddr;

                free(Msg);

                return true;
                
            }
        }
    }

    return false;
            
}


void NODE::SendPairRequest()
{  
    uint8_t msg = REQUEST_UPLINK;
    
    if(!PairWait && !(candidate.empty()))
    {

        Serial.print("sono in sendUplinkRequest\n");    
        auto first = candidate.begin();

        if((role == middle) || (role == leaf))
        {
            if(levelParentNode > first->current_level)
            {
                PairWait = true;
                timeRequest = 0;

                SendToPan((uint8_t*)&msg,1,first->address,false);
                startedNet = false;        
            }
            else
            {
                candidate.clear();
                FramePresentation = true;
                PairWait = false;
                timeRequest = 0;
            }
        }

        else //is a idle obj or orphan obj
        {
            if(net->currentLayer == 0) //new entry
            {
                Serial.println("mando il messaggio al mio nuovo genitore!");
                SendToPan((uint8_t*)&msg,1,first->address,false);
                PairWait = true;
                timeRequest = 0;
            }
            else if(getCurrentMeshLevels() <= (net->MaxLevelTree-1)) //orphan obj
            {
                if(first->current_level <= net->currentLayer)
                {
                    SendToPan((uint8_t*)&msg,1,first->address,false);
                    PairWait = true;
                    timeRequest = 0;
                }
                    
                else
                {
                    candidate.clear();
                    FramePresentation = true;
                    timeRequest = 0;      
                }        
            }
            else //levels full
            {
                //modify RSSI threshold
                if(getThreshold() > 0.2)
                    setThreshold(getThreshold()-0.1);
                
                candidate.clear();
                FramePresentation = true;
                timeRequest = 0;
    
            }
        }
    }
}

void NODE::ReceiveRequest(uint8_t * msg, uint16_t const &addr)
{
    //Serial.print("sono entrato in receiveRequest\n");
    //request_t req;
    uint8_t dispatch = *msg & 0xFC;
    uint16_t msgReply = 0;
    switch (dispatch)
    {
        case CHECK_ROOT:
            if(role == lead)
            {
                ((uint8_t*)(&msgReply))[0] = ELECTION_COMPLETE;
                SendToPan((uint8_t*)&msgReply,1,addr,false);
            }
            break;
        case HELLO_NODE:

            if((rootFound) && (role != idle) && (role != gateway))
            {
                
                ((uint8_t*)(&msgReply))[0] = HELLO_REPLY;

                if(addr == getParentNode())
                {
                    /*if the parent node is back*/
                    if(net->currentLayer <= net->MaxLevelTree-1)
                    {
                        if(net->currentLayer == net->MaxLevelTree-1)
                            role = leaf;
                        else
                        {     
                            role == middle;
                            FramePresentation = true;
                            cnt_fault_node = 0;
                        }
                    }
                        
                    /*-------------------------*/

                    if(net->currentLayer = 1)
                        ((uint8_t*)(&msgReply))[0] |= 0x01; //lead
                    else
                        ((uint8_t*)(&msgReply))[0] |= 0x02; //middle

                    ((uint8_t*)(&msgReply))[1] = nlinkParentNode << 4 | \
                    levelParentNode;
                    
                    SendToPan((uint8_t*)&msgReply,2,addr,false);

                    //delay(20);
                    
                    if(net->getRoutingTable()->GetTableSize() != 0)
                        SendRoutingTableToParantNode();
                    else
                    {
                        msgReply = ROUTING_UPDATE;
                        SendToPan((uint8_t*)&msgReply,1,getParentNode(),false);
                    }
                }
                else if (net->getRoutingTable()->checkDirectChild(addr))
                {
                    ((uint8_t*)(&msgReply))[0] |= 0x03; //child
                    ((uint8_t*)(&msgReply))[1] = net->currentDwlink << 4 | \
                    net->currentLayer;

                    SendToPan((uint8_t*)&msgReply,2,addr,false);
                }
                else
                {
                    SendToPan((uint8_t*)&msgReply,1,addr,false);
                }
            }

            else if(role == gateway)
            {
                if(!(msg[0] & 0x03))
                {
                    ((uint8_t*)(&msgReply))[0] = GATEWAY_DISPATCH;
                    SendToPan((uint8_t*)&msgReply,1,BROADCAST_ADDRESS,false);
                }
            }
            break;

        case HELLO_REPLY:
            
            rootFound = true;
            startedNet = true;

            if((*msg & 0x03) == 1)
            {
                role = lead;

                net->currentLayer = msg[1] & 0x0f;
                net->currentDwlink = (msg[1] & 0xf0) >> 4;
                
            }
            else if((*msg & 0x03) == 2)
            {
            role = middle;
            net->currentLayer = msg[1] & 0x0f;
            net->currentDwlink = (msg[1] & 0xf0) >> 4;
            }
            else if((*msg & 0x03) == 3)
            {
                role = middle;
                setParentNode(addr);
                levelParentNode = msg[1] & 0x0f;
                nlinkParentNode = (msg[1] & 0xf0) >> 4;
            }    
            break;

        case REQUEST_UPLINK:

            ((uint8_t*)(&msgReply))[0] = UPLINK_REPLY;
            Serial.printf("Richiesta di associazione da %x\n",addr);
            if(!(net->getRoutingTable()->checkDirectChild(addr))) //check if direct child
            {
                
                if(net->currentDwlink < net->MaxDwlink)
                {
                    if(net->getRoutingTable()->checkChild(addr))
                    {
                        Serial.println("non è un mio diretto!");
                        net->getRoutingTable()->DelRecord(addr);
                    }
                            
                    net->currentDwlink++;
            
                    AddRecordTable(addr);
                    
                    ((uint8_t*)(&msgReply))[0] |= 1;

                }
                
            }
            else
            {
                ((uint8_t*)(&msgReply))[0] |= 1;
    
            }

            
            SendToPan((uint8_t*)&msgReply,1,addr,false);
        
                                    
            if((role != lead) && (((msgReply & 0x00ff) & 0x03) != 0))
            {
                Serial.print("SONO QUIIIIIIIIIIIII!\n");
                if(net->getRoutingTable()->GetTableSize() != 0)
                    SendRoutingTableToParantNode();
                else
                {
                    ((uint8_t*)(&msgReply))[0] = ROUTING_UPDATE;
                    SendToPan((uint8_t*)&msgReply,1,getParentNode(),false);
                }    
            }    

            break;
        case UPLINK_REPLY:
            
            if((*msg & 0x03) == 1) //true reply
            {
                if(role == middle || role == leaf)
                {
                    msgReply = ROUTING_DELETE;
                    SendToPan((uint8_t*)&msgReply,1,getParentNode(),false);
                }
                
                //change parent obj or new parent obj
                setParentNode(addr);

                
                levelParentNode = candidate.begin()->current_level;
                net->currentLayer = (candidate.begin()->current_level)+1;

                if(net->currentLayer < (net->MaxLevelTree-1))
                    role = middle;
                else
                {
                    role = leaf;
                }
                
            
                if(role != leaf)
                {
                    SendRoutingTableToParantNode();
                }
                
                FramePresentation = true;
                candidate.clear();
                timePresentation = 0;
                timeCheckPresent = 0;
                cnt_fault_node = 0;
                startedNet = true;          
            }
            else if((*msg & 0x03) == 0)
            {
                if(role == middle)
                    startedNet = true;
            } 
        
            break;

        case ROUTING_DELETE:
            UpdateRoutingTable(msg,addr,0);
            if(role != lead)
            {
                SendRoutingTableToParantNode();
            }
            break;
        
        case HEARTBEAT:
            //listen own child or parent obj
            if(getParentNode() == addr)
                cnt_fault_node = 0;
            else    
                net->getRoutingTable()->resetLifeTime(addr);

            break;
        case ELECTION_COMPLETE:
        if(role == gateway)
        {
            rootAddr = addr;
            rootFound = true;
            Serial.printf("root addr: %x\n", rootAddr);
        }
        else if((role != idle) && (levelParentNode == 1))
        {
            startedNet = true;
            Serial.println("il mio genitore è adesso una root!");
        }
        
        break;

        case SEND_GW_COMPLETE:
            
        msgReply = REQUEST_REPLY;
        Serial.println("HO RICEVUTO IL SEND GW COMPLETE!");
    
        if(SendGw)
        {
            /*yes*/
            msgReply |= 1;
            SendToPan((uint8_t*)&msgReply,1,addr,false);
        }
        else
        {
            /*no*/
            SendToPan((uint8_t*)&msgReply,1,addr,false);
        }
            
        break;

        default:
            break;
        
    }
}


void NODE::setTimer()
{
    
    if(FramePresentation && (net->currentLayer > 0 || rootFound))
    {
        timePresentation++;
        
        if(role != lead)
            timeCheckPresent++;

    }

    if(!FramePresentation)    
        timeRequest++;

    if(role == idle)
        timeStartWait++;


    if(startElection)
    {
        timerElection++;
    }

    if(role != gateway)
    {
        HeartBeatCount++;
    }
    
    //timerVoters += time;
    
    if(startedNet)
        cnt_fault_node++;

    
    
    //Serial.println("Controllo il buffer!");
    net->cleanTimeoutMessage();

    if(cleanTimeoutRoutingTable())
    {
        if(role != lead)
            SendRoutingTableToParantNode();
    }
    

    if(net->getRoutingTable()->GetTableSize() != 0)
        net->getRoutingTable()->PrintRoutingTable();

}


void NODE::SelfOrganization(uint8_t (*AppSensor)(void))
{
    check_events();
   
    if(timerFires)
   {
      #ifdef ESP32
      portENTER_CRITICAL(&MuxTimer);
      #endif

      timerFires = false;
      
      #ifdef ESP32
      portEXIT_CRITICAL(&MuxTimer);
      #endif

      setTimer();

   }

    
    if(role == gateway)
    {
        if(!startedNet)
        {
            Serial.println("sto inviando!");
            uint8_t msg = CHECK_ROOT;
            SendToPan(&msg,1,BROADCAST_ADDRESS,false);
            startedNet = true;
        }    
        return;
    }

    if((rssiGateway == 0) && !(rootFound))
    {
        Serial.println("Datemi un RSSI!"); 
        Serial.println("Hello there!");

        uint8_t msg = HELLO_NODE;

        SendToPan((uint8_t*)&msg,1,BROADCAST_ADDRESS,false);
        delay(200);

    }
    
    if(!startElection)
    {
        //delay(5000);
        //Serial.println("sono dentro start Election!");
        if((role == idle) && !(rootFound) && (timeStartWait < 20))
        {
            if( (millis() - timePred) > 500)
            {
                Serial.println("Hello there!");

                uint8_t msg = HELLO_NODE;

                if(rssiGateway != 0)
                {
                    msg |= 0x01;
                    //per avvisare al gateway di non mandare più l'rssi
                }

                //net->Send((uint8_t*)&msg,1,BROADCAST_ADDRESS,false);
                //delay(10000);
                timePred = millis();
            }
            
            if(!voters.empty())
            {
                startElection = true;
            }
                //timeStartWait = 10;
        }

        
        if((timeStartWait) >= 20 && (role == idle) && !(rootFound)) //20 seconds
        {
            if(candidate.empty())
            {
                if(rssiGateway == 0 && voters.empty())
                {
                    timeStartWait = 0;
                }
                else
                    startElection = true;
            }
            else
            {
                rootFound = true;
            }             
        }

        if(!(candidate.empty()) && (role != lead))
        {
            //Serial.printf("Root trovata! %d\n", obj->rootFound);
        // Serial.println("HO RICEVUTO DEI POTENZIALI GENITORI!");
            if(FramePresentation && role != lead)
            {
                if(timeCheckPresent > 40)
                {
                    Serial.println("sto settando il boolean della frame a false!");
                    FramePresentation = false;
                    timeCheckPresent = 0;
                    timeRequest = 0;
                }
            }    
        }
        else
        {
            if((role == idle) && (candidate.empty()))
            {
                if(getCurrentMeshLevels() >= net->MaxLevelTree-1)
                {
                    //modify RSSI threshold
                    if(getThreshold() > 0.2)
                        setThreshold(getThreshold()-0.1);
                }
            }
        }

        if(!FramePresentation)
        { 
            //Serial.println("HO DELLE RICHIESTE DI ASSOCIAZIONE!");
            if(timeRequest > 60)
            {
                PairWait = false;
                if(!candidate.empty())
                    candidate.erase(candidate.begin());
                else
                    FramePresentation = true;
            }

            SendPairRequest();
        }


        checkFaultNode();        
    }
    
    if(timePresentation > 20)
    {
        if((role != idle) && (role != leaf))
        {
            Serial.println("STO MANDANDO LE FRAME DI PRESENTAZIONE!");
            sendPresentationFrame();
        }

        timePresentation = 0;
    }

    
    if(startElection)
    {
        electionRoot(5);
    }

    if((HeartBeatCount >= 20) && ((rootFound) || (net->currentLayer > 0) ))  
    {
        if((net->getRoutingTable()->GetTableSize() != 0) || (getParentNode() != 0))
        {
            /*send heartbeat message to each 30 seconds*/
            
            uint16_t msg = HEARTBEAT;
            Serial.println("ho inviato un heartBeat!");
            SendToPan((uint8_t*)&msg,1,BROADCAST_ADDRESS,false);
            HeartBeatCount = 0;
            
            if(role != lead && role != idle)
            {
                Serial.println("invio la temperatura!");
                /*SEND VALUE SENSOR TO PARENT NODE*/
                ((uint8_t*)(&msg))[0] = LISTMESSAGE;
                ((uint8_t*)(&msg))[1] = (*AppSensor)();

                SendToPan((uint8_t*)&msg,2,0,false); //addr 0 send message to lead node
            }   
        }
    }        
}


void NODE::checkFaultNode()
{
    if(role != lead)
    {
        if((millis() - TimeProva) > 60000 )
        {
            Serial.printf("cnt_fault is: %d\n",cnt_fault_node);
            
            if((cnt_fault_node >= 60) && (cnt_fault_node < 120))
                Serial.println("Non trovo il mio genitore!");
        
            TimeProva = millis();
        }
        if((cnt_fault_node >= 60) && (cnt_fault_node < 120))
        {        
            role = idle;
            FramePresentation = false;
            candidate.clear();
        }
            
        else if(cnt_fault_node > 130) //timer expired
        {
            if(levelParentNode == 0) //root failed
            {
                Serial.println("elezione della nuova root!");
                voters.clear();
                //votes.clear();
                rootFound = false;
                //electionRoot(5);
                startElection = true;
            
            }
            else
            {
                setParentNode(0);
                FramePresentation = true;
            }

            cnt_fault_node = 0;
            //startedNet = false;

        }
    }
    
}

void NODE::sendPacketProva()
{
    uint8_t * buf = (uint8_t*)calloc(255,sizeof(uint8_t));
    
    for (int i = 0; i < 255; i++)
    {
        buf[i] = i;
    }
    
    SendToPan(buf,255,BROADCAST_ADDRESS,true);
    
    
}


void NODE::ReceivePacketProva(uint8_t * msg)
{
    //int * array;
    //while((array = (int*)calloc(255,sizeof(int))) == NULL);
    //memcpy(array,(int*)msg,255*sizeof(int));

    //free(msg);

    Serial.println("\nil messaggio ricevuto è:");

    for (int i = 0; i < 255; i++)
    {
        Serial.printf("%d  ",msg[i]);
        //Serial.print("\t");
    }

    free(msg);

    Serial.println();
    
}


#ifdef ESP32

void IRAM_ATTR NODE::NetInterrupt()
{
    
    xSemaphoreGiveFromISR(xSemaphore,NULL);
    
}


void InterruptTask( void * arg)
{
    Lowpan * pan = (Lowpan*)arg;
    
    while(1)
    {
        if(xSemaphoreTake(xSemaphore,portMAX_DELAY) == pdTRUE)
        {
            pan->getMac()->interrupt_handler();              
        }
    }
}

#endif


#ifdef ESP8266
void ICACHE_RAM_ATTR NODE::NetInterrupt()
{
    net->getMac()->interrupt_handler(); 
}
#endif



