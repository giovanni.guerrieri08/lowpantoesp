#ifndef __ROUTE_H__
#define __ROUTE_H__

#include <iostream>
#include <vector>
#include <Arduino.h>

using namespace std;

#define ROUTING_UPDATE 0x78 //011110xx
#define ROUTING_DELETE 0x60 //011000xx

typedef struct 
{
    uint16_t dest_addr;
    uint8_t hop;
}record_t;

class Route
{
    public:
        Route();
        ~Route();
       
        void Update(uint8_t *, uint16_t const &, uint16_t const &);
        uint8_t * PrepareRoutingPacket();
        uint16_t GetPacketSize();
        void AddRecord(uint16_t const &addrDest, uint16_t const &nextHop, uint16_t const &hop);

        bool checkDirectChild(uint16_t const &addr);
        bool checkChild(uint16_t const &addr);
        void PrintRoutingTable();
        
        uint16_t getNextHopAddress(uint16_t const &);

        uint16_t getMaxHops();

        void resetLifeTime(uint16_t const &);

        int checkLifeTimeRecord();

        uint16_t GetTableSize();

        void DelRecordAll(uint16_t const &);

        void DelRecord(uint16_t const &);
        
        uint16_t getAddrParentNode();

        void setParentNode(uint16_t addr);

        vector<vector<uint16_t>> table;
   
    private:
       

       uint16_t AddrParentNode;
           
};


#endif