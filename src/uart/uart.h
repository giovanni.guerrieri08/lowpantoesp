#ifndef __UART_H__
#define __UART_H__


#include "Arduino.h"

#define DELIMITER_START 126
#define FIRST_DELIMITER_END 13
#define LAST_DELIMITER_END 10


class Uart
{

    public:
        Uart(HardwareSerial *);
        ~Uart();

        uint8_t calcChecksum(uint8_t * data,uint16_t length);
        void senData(uint8_t * data, uint16_t length);
        
        bool AvailableData();

        bool checkChecksum(uint8_t * data, uint16_t len);

        uint8_t rxData[1024];
        uint16_t length;

        bool nextMessage = false;

    private:
       
        HardwareSerial * ser;
        void sendAck(bool);

        


};


#endif