#include "uart.h"

uint8_t rxMessage[50];

uint8_t * txPacket;
uint16_t lenTxPacket;

uint16_t cntcar = 0;
bool tilde = false;

Uart::Uart(HardwareSerial * ser)
{
    this->ser = ser;
}

Uart::~Uart(){}


void Uart::senData(uint8_t* data, uint16_t length)
{
    
    int offset = 0;
    uint8_t dataBuf[length+7];
    
    dataBuf[offset++] = DELIMITER_START;

    memcpy(&dataBuf[offset], (uint8_t*)&length,sizeof(uint16_t));
    offset += sizeof(uint16_t);

    dataBuf[offset++] = 1;

    memcpy(&dataBuf[offset],data,length);
    offset+=length;

    dataBuf[offset++] = calcChecksum(dataBuf,length);

    dataBuf[offset++] = FIRST_DELIMITER_END;
    dataBuf[offset++] = LAST_DELIMITER_END;
    
    ser->write(dataBuf,offset);

    /*save the last message sent until the ack is received*/ 
    txPacket = (uint8_t*)calloc(offset,sizeof(uint8_t));
    memcpy(txPacket,dataBuf,offset);

    lenTxPacket = offset;

    nextMessage = false;
    
    
}


uint8_t Uart::calcChecksum(uint8_t * data, uint16_t length)
{
    
    uint16_t sum = 0;

    for (int i = 0; i <= length; i++)
    {
        sum += data[i+3];
    }
    
    return 0xff - (sum & 0x00ff);
}


bool Uart::AvailableData()
{
    
    if(ser->available())
    {  
        byte x = ser->read();
        if(x == DELIMITER_START || tilde)
        {
            if(!tilde)
                tilde = true;
            
            rxMessage[cntcar++] = x;

            if(cntcar > 1 && cntcar < 3)
            {
                memcpy(&length,(uint16_t*)&rxMessage[1],sizeof(uint16_t));
            }

            if(cntcar <= length+7)
            {
                if(rxMessage[cntcar-1] == LAST_DELIMITER_END && \
                rxMessage[cntcar-2] == FIRST_DELIMITER_END)
                {
                    if(checkChecksum(rxMessage,length))
                    {
                        if(rxMessage[3] == 0) //is ack
                        {
                            if(rxMessage[4] == 1) //reply response
                            {
                                if(txPacket != NULL)
                                {
                                    free(txPacket);
                                    txPacket = NULL;
                                }    
                            }
                            else //resend the packet (ack false)
                            {
                                if(txPacket != NULL)
                                    ser->write(txPacket,lenTxPacket);
                                else
                                    sendAck(true);
                            }
                        }
                        else //data packet
                        {
                            memcpy(rxData,&rxMessage[4],length);

                            for (int i = 0; i < length; i++)
                            {
                                rxData[i] -= 48;;
                            }
                            

                            memset(rxMessage,0,sizeof(rxMessage));
                            tilde = false;
                            cntcar = 0;

                            sendAck(true);
                            //delay(100);

                            
                            return true;
                        }            
                    }
                    else
                    {
                        sendAck(false);     
                    }

                    memset(rxMessage,0,sizeof(rxMessage));
                    tilde = false;
                    cntcar = 0;  
                }
            }
            else
            {
                //Serial.print("NON L'HO RICEVUTO CORRETTAMENTE");
                sendAck(false);
                
                tilde = false;
                cntcar = 0;
                memset(rxMessage,0,sizeof(rxMessage));
            }
                
        }           
    }

    return false;
}

bool Uart::checkChecksum(uint8_t * data, uint16_t len)
{
    uint8_t checksumReceived = data[4+len];

    uint16_t sum = 0;
    for (int i = 0; i <= length; i++) // <= because start from type message
    {
        sum += data[i+3];
    }

    sum+=checksumReceived;

    return (sum & 0x00ff == 0xff);
    
}


void Uart::sendAck(bool type)
{
    uint8_t buffer[8];

    buffer[0] = DELIMITER_START;
    buffer[1] = 1;
    buffer[2] = 0;
    buffer[3] = 0;
    buffer[4] = type;
    buffer[5] = calcChecksum(buffer,1);
    buffer[6] = FIRST_DELIMITER_END;
    buffer[7] = LAST_DELIMITER_END;

    ser->write(buffer,8);

    txPacket = (uint8_t*)calloc(8,sizeof(uint8_t));
    memcpy(txPacket,buffer,8);

}

