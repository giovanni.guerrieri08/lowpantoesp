#ifndef __LOWPAN_H__
#define __LOWPAN_H__

/*this library corresponds to the third level*/

#include "mrf24j4.h"
#include "list"
#include "pan_def.h"

typedef struct MsG //struct to handle messages larger than mac payload
{
    uint16_t addr;
    uint8_t timeout = 50; //if timeout equal zero, the incomplete message is removed
    uint8_t * msg;
    uint8_t n_frag; //if n_frag equal last_nseq return msg
    uint8_t first_nseq;
    uint8_t last_nseq;
    uint16_t len;

}MsG_t;


class Lowpan 
{
    public:
        Lowpan();
        
        ~Lowpan();
            
        void init(int panid, int channel, uint16_t &address);
        
        void Send(uint8_t *message, uint16_t const &dim, uint16_t const &addr,bool heapfree);
        
        void cleanTimeoutMessage();
       
        Mrf24j * getMac();
        
        void SendToLoWPAN(uint8_t *, uint8_t const &);

        void onReceive();

        Route * getRoutingTable();

        uint8_t * MessageProcess(rx_msg_t *);

        list <rx_msg_t*> fifo;

        uint8_t currentLayer = 0;
        uint8_t currentDwlink = 0;

        uint8_t MaxLevelTree = 6;
        uint8_t MaxDwlink = 6;
        
    private:
       
            
        Mrf24j * mrf;

        vector <vector <MsG_t>> bufferMsg;
 
};


#endif