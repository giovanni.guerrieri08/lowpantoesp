#include "node.h"
#include <iterator>

void Lowpan::onReceive()
{
    
    uint16_t dest_addr_mac = mrf->get_rxinfo()->frame.DataFrame.dest_addr;
    uint16_t src_addr_mac = mrf->get_rxinfo()->frame.DataFrame.src_addr;

    
    if(mrf->get_rxinfo()->frame.DataFrame.dest_panid == mrf->get_pan())
    {
        if((dest_addr_mac == mrf->address16_read()) || (dest_addr_mac == BROADCAST_ADDRESS))
        {
            digitalWrite(LEDRECV,1);
        
            msg_t * msg = (msg_t*)mrf->get_rxinfo()->frame.DataFrame.payload;


            if((msg->destAddr == mrf->address16_read()) || (msg->destAddr == BROADCAST_ADDRESS))
            {
                rx_msg_t * msgRecv = (rx_msg_t*)calloc(1,sizeof(rx_msg_t));

                memcpy(&msgRecv->message,(msg_t*)&mrf->get_rxinfo()->frame.DataFrame.payload[0],mrf->rx_datalength());

                msgRecv->lqi = mrf->get_rxinfo()->lqi;
                msgRecv->rssi = mrf->get_rxinfo()->rssi;

                fifo.push_back(msgRecv);

            }
        
            else
            {
                /*check the routing table to forward the message*/
                
                uint16_t len = mrf->rx_datalength();
                
                if(src_addr_mac == mrf->getRoutingTable()->getAddrParentNode())
                {
                    uint16_t nextHop = mrf->getRoutingTable()->getNextHopAddress(msg->destAddr);

                    if(nextHop != NULL)
                        mrf->SendPacketToMAC(mrf->get_rxinfo()->frame.DataFrame.payload,len,nextHop);
                }
                
                else
                {
                    if(mrf->getRoutingTable()->checkDirectChild(src_addr_mac))
                    {
                        if(currentLayer != 1)
                            mrf->SendPacketToMAC(mrf->get_rxinfo()->frame.DataFrame.payload,len,mrf->getRoutingTable()->getAddrParentNode());
                        else
                        {
                            msg->destAddr = mrf->getRoutingTable()->getAddrParentNode();
                            mrf->SendPacketToMAC((uint8_t*)&msg,len,mrf->getRoutingTable()->getAddrParentNode());
                        }
                        
                    }
                }      
            }

            digitalWrite(LEDRECV,0);
        }    
    }
}


Lowpan::Lowpan() {}

Lowpan::~Lowpan() {}

void Lowpan::init(int panid, int channel, uint16_t &address)
{
    mrf = new Mrf24j();
    
    mrf->reset();
    mrf->init();
    mrf->set_pan(panid);
    mrf->set_channel(channel);
    mrf->address16_write(address);
    mrf->set_promiscuous(true);
   
}


Mrf24j * Lowpan::getMac()
{
    return mrf;
}

Route * Lowpan::getRoutingTable()
{
    return mrf->getRoutingTable();
}


void Lowpan::Send(uint8_t *message, uint16_t const &dim, uint16_t const &addr,bool heapfree = true)
{
    if(message != NULL)
    {    
        msg_t msg;
        uint16_t len = 0;
        
        msg.destAddr = addr;
        msg.srcAddr = mrf->address16_read();
        
        if(dim > sizeof(msg.body.normal.payload))
        {
            uint16_t offset = 0;
            
            bool firstFrag = true;
        
            do
            {
                if(firstFrag)
                {
                    
                    msg.fragment = DISPATCH_FRAG1;

                    /*len packet*/
                    msg.fragment |= (dim & 0x700) >> 8;
                    msg.lenMessage = dim & 0x00FF;  
        
                    msg.body.firstfrag.numFragment = mrf->getNumSeq();
                    //bufferSend[numPacketSend][i++] |= ((dim << 3) & 0xF800) >> 11;
                    //bufferSend[numPacketSend][i++] = dim & 0x00FF;
                    
                    
                    //offset += (payload-i);
                    offset += sizeof(msg.body.firstfrag.payload);

                    //memcpy(&frag[i],(const uint8_t *)&message[0],payload-i);
                    memcpy(&msg.body.firstfrag.payload[0],(const uint8_t *)&message[0],\
                    sizeof(msg.body.firstfrag.payload));

                    //memcpy(&frag[i],message,payload);
                    len = sizeof(msg_t);
    
                    firstFrag = false;

                }

                else
                {
                    
                    msg.fragment = DISPATCH_FRAGN;
                    /*len packet*/
                    
                    msg.fragment |= (dim & 0x700) >> 8;
                    msg.lenMessage = dim & 0x00FF;
                    
                    //bufferSend[numPacketSend][i++] |= ((dim << 3) & 0xF800) >> 11;
                    //bufferSend[numPacketSend][i++] = dim & 0x00FF;
                    
                    msg.body.nextfrag.offset = offset;
                    msg.body.nextfrag.numFragment = mrf->getNumSeq();
                   
                     
                    if((dim-offset) >= sizeof(msg.body.nextfrag.payload))
                    {
                        memcpy(&msg.body.nextfrag.payload[0],(const uint8_t *)&message[offset],\
                        sizeof(msg.body.nextfrag.payload));
                        
                        offset += sizeof(msg.body.nextfrag.payload);
                        len = sizeof(msg_t);
                    }
                    else
                    {
                        //len = dim-offset;
                        uint8_t remaining_part = dim-offset;
                        uint8_t unused_part = sizeof(msg.body.nextfrag.payload) - remaining_part;
                        
                        memcpy(&msg.body.nextfrag.payload[0],(const uint8_t *)&message[offset],\
                        remaining_part);

                        offset += remaining_part;

                        len = sizeof(msg_t)-unused_part;

                    } 
                }

                //try to send!
                if(addr != 0)
                    mrf->SendPacketToMAC((uint8_t*)&msg,len,addr);
                else
                {
                    if(currentLayer != 1)
                        mrf->SendPacketToMAC((uint8_t*)&msg,len,mrf->getRoutingTable()->getAddrParentNode());
                    else
                    {
                        msg.destAddr = mrf->getRoutingTable()->getAddrParentNode();
                        mrf->SendPacketToMAC((uint8_t*)&msg,len,mrf->getRoutingTable()->getAddrParentNode());
                    }
                    
                }
                

            }while(offset != dim);
        }
        else
        {
            msg.fragment = 0;
           
            msg.lenMessage = dim;
            memcpy(&msg.body.normal.payload[0],&message[0],dim);


            len = sizeof(msg_t)-(sizeof(msg.body.normal.payload)-dim);

            if(addr != 0)
                mrf->SendPacketToMAC((uint8_t*)&msg,len,addr);
            else
            {
                Serial.printf("il mio parent node e': %x\n",mrf->getRoutingTable()->getAddrParentNode());
                if(currentLayer != 1)
                    mrf->SendPacketToMAC((uint8_t*)&msg,len,mrf->getRoutingTable()->getAddrParentNode());
                else
                {
                    msg.destAddr = mrf->getRoutingTable()->getAddrParentNode();
                    mrf->SendPacketToMAC((uint8_t*)&msg,len,mrf->getRoutingTable()->getAddrParentNode());
                }
            }    
        }

        if(heapfree)
            free(message);
    }    
}



bool checkSequenceNumberInterval(const MsG_t &a, uint8_t nseq, uint16_t len)
{
    
    if(a.len == len)
    {
        uint8_t value1 = 0;
        
        if( (nseq > a.first_nseq) && (nseq < a.last_nseq) ) 
            return true;

        
        if( (nseq == a.first_nseq) || (nseq == a.last_nseq))
            return true;

        
        if((a.last_nseq - a.first_nseq) < 0)
            value1 = (a.last_nseq - a.first_nseq)+256;
        else
            value1 = a.last_nseq - a.first_nseq;
         

        if( (a.first_nseq > nseq) && (nseq < a.last_nseq) )
        {
            uint8_t value = 0;

            if( (nseq - a.first_nseq) < 0 )
                value = (nseq-a.first_nseq)+256;
            else
                value = nseq - a.first_nseq;
            
            if( (value%256) < (value1%256) )
                return true;
        }
            

        if( (a.first_nseq < nseq) && (nseq > a.last_nseq) )
        {
            uint8_t value = 0;

            if( (a.last_nseq - nseq) < 0 )
                value = (a.last_nseq - nseq)+256;
            else
                value = a.last_nseq - nseq;

            if( (value%256) < (value1%256) ) 
                return true;
        }    
    }

    return false;
      
}


void MakeIntervalSequenceNumber(MsG_t &a,msg_t *msg, uint16_t const &len)
{
    Serial.printf("\nlen è di: %d\n", len);
    if((msg->fragment & 0xE0) != DISPATCH_FRAG1)
    {
        if((len - (msg->body.nextfrag.offset+1)) <= sizeof(msg->body.nextfrag.payload)) //this nseq is last fragment
        {
            Serial.println("intervallo nel primo if");
        
            a.last_nseq = msg->body.nextfrag.numFragment;
            uint16_t lenTotMiddleSegment = (len-(len-(msg->body.nextfrag.offset+1))-1)-sizeof(msg->body.firstfrag.payload); //114 is len first segment
            uint8_t nRemainPacket = (lenTotMiddleSegment/sizeof(msg->body.nextfrag.payload))+1; //1 is the first fragment
            
            uint8_t tmp = 0;
            if((msg->body.nextfrag.numFragment - nRemainPacket) < 0 )
                tmp = (msg->body.nextfrag.numFragment - nRemainPacket) + 256;
            else
                tmp = msg->body.nextfrag.numFragment - nRemainPacket;

            a.first_nseq = tmp % 256;
        }   
        else //this nseq doesn't last fragment
        {
            Serial.println("intervallo nel secondo if");
            
            uint16_t lenSegPrev = msg->body.nextfrag.offset;
            uint8_t nPrevPacket = 0;
            
            if( (lenSegPrev - sizeof(msg->body.firstfrag.payload)) == 0 )
                nPrevPacket = 1;
            else
                nPrevPacket = ( (lenSegPrev - sizeof(msg->body.firstfrag.payload))/sizeof(msg->body.nextfrag.payload) )+1;
            
            uint8_t tmp = 0;
            if((msg->body.nextfrag.numFragment - nPrevPacket) < 0 )
                tmp = (msg->body.nextfrag.numFragment - nPrevPacket) + 256;
            else
                tmp = msg->body.nextfrag.numFragment - nPrevPacket;

            a.first_nseq = tmp % 256;

            uint16_t lenActualSeg = msg->body.nextfrag.offset+sizeof(msg->body.nextfrag.payload);

            float b = (len - lenActualSeg) /((float)sizeof(msg->body.nextfrag.payload));
            uint8_t nLastPackets = (uint8_t)ceil(b);

            a.last_nseq = (msg->body.nextfrag.numFragment + nLastPackets) % 256;
        }
        
    }
    else //this is the first fragment
    {
        Serial.println("intervallo nel terzo if");
        a.first_nseq = msg->body.firstfrag.numFragment;
        float b = (len - sizeof(msg->body.firstfrag.payload)) / ((float)sizeof(msg->body.nextfrag.payload));
        uint8_t nRemainPacket = (uint8_t)ceil(b);

        a.last_nseq = (msg->body.firstfrag.numFragment + nRemainPacket) % 256;
        
    }

    a.n_frag = a.first_nseq;
    Serial.printf("\nINTERVAL( begin: %d ed end: %d)\n", a.first_nseq,a.last_nseq);
}


void initMessage(MsG_t &MSG, uint16_t const &len, uint16_t const &addr)
{
    MSG.len = len;
            
    MSG.msg = (uint8_t*)calloc(len,sizeof(uint8_t));
    MSG.addr = addr;

}


uint8_t * Lowpan::MessageProcess(rx_msg_t * msg)
{
    uint8_t * MSG = NULL;

    if( ((msg->message.fragment & 0xE0) == DISPATCH_FRAG1) || ((msg->message.fragment & 0xE0) == DISPATCH_FRAGN))
    {
        bool firstFrag = false;
        uint16_t srcAddr = msg->message.srcAddr;
        uint8_t nseq = 0;

        if((msg->message.fragment & 0xE0) == DISPATCH_FRAG1)
        {
            firstFrag = true;
            nseq = msg->message.body.firstfrag.numFragment;
        }
        else
            nseq = msg->message.body.nextfrag.numFragment;

            
        
        uint16_t lenMessage = ((msg->message.fragment & 0x07) << 8) | msg->message.lenMessage;

        auto foundVector = find_if(bufferMsg.begin(),bufferMsg.end(),[srcAddr](const vector <MsG_t> &a)\
        {return (srcAddr == a.begin()->addr);});

        if(foundVector != bufferMsg.end())
        {
            
            auto foundElement = find_if(foundVector->begin(), foundVector->end(), [nseq,lenMessage](const MsG_t &a)\
            {return checkSequenceNumberInterval(a,nseq,lenMessage);});

            if(foundElement != foundVector->end())
            {
            
                if(firstFrag)
                    memcpy(&foundElement->msg[0],&msg->message.body.firstfrag.payload[0],sizeof(msg->message.body.firstfrag.payload));
                else
                {
                    if( (lenMessage - msg->message.body.nextfrag.offset) >= sizeof(msg->message.body.nextfrag.payload) )
                        memcpy(&foundElement->msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                        sizeof(msg->message.body.nextfrag.payload));
                    else
                        memcpy(&foundElement->msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                        (lenMessage - msg->message.body.nextfrag.offset));
                }

                foundElement->n_frag += 1;
                Serial.println("\nFound element!");
                Serial.printf("nfrag é: %d, nseq è %d, addr é %x\n",foundElement->n_frag,nseq,msg->message.srcAddr);

                if(foundElement->n_frag == foundElement->last_nseq)
                {
                    
                    Serial.printf("finito per %x\n", msg->message.srcAddr);
                    
                    /*msg applicativo*/
                    MSG = foundElement->msg;
                    
                    foundVector->erase(foundElement);

                    if(foundVector->empty())
                        bufferMsg.erase(foundVector);
                    
                }
            }
            else //put in the vector of the messages
            {
                MsG_t MSG;
                
                initMessage(MSG,lenMessage,srcAddr);

                MakeIntervalSequenceNumber(MSG,&msg->message,lenMessage);

                Serial.println("\nNew message!");    
                Serial.printf("nfrag é: %d, nseq è %d, addr é %x\n",MSG.n_frag,nseq,msg->message.srcAddr);

                if(firstFrag)
                    memcpy(&MSG.msg[0],&msg->message.body.firstfrag.payload[0],sizeof(msg->message.body.firstfrag.payload));
                else
                {
                    if( (lenMessage - msg->message.body.nextfrag.offset) >= sizeof(msg->message.body.nextfrag.payload) )
                        memcpy(&MSG.msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                        sizeof(msg->message.body.nextfrag.payload));
                    else
                        memcpy(&MSG.msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                        (lenMessage - msg->message.body.nextfrag.offset));
                }

                foundVector->push_back(MSG);
            }
        }
        else
        {
            vector <MsG_t> tmp;

            MsG_t MSG;
            
            initMessage(MSG,lenMessage,msg->message.srcAddr);
                    
            MakeIntervalSequenceNumber(MSG,&msg->message,lenMessage);
            
            Serial.println("\nNew first message!");    
            Serial.printf("nfrag é: %d, nseq è %d, addr é %x\n",MSG.n_frag,nseq,msg->message.srcAddr);

            if(firstFrag)
                memcpy(&MSG.msg[0],&msg->message.body.firstfrag.payload[0],sizeof(msg->message.body.firstfrag.payload));
            else
            {
                if( (lenMessage - msg->message.body.nextfrag.offset) >= sizeof(msg->message.body.nextfrag.payload) )
                    memcpy(&MSG.msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                    sizeof(msg->message.body.nextfrag.payload));
                else
                    memcpy(&MSG.msg[msg->message.body.nextfrag.offset],&msg->message.body.nextfrag.payload[0],\
                    (lenMessage - msg->message.body.nextfrag.offset));
            }


            tmp.push_back(MSG);

            bufferMsg.push_back(tmp); 

        }
    }
    else
    {
        MSG = (uint8_t*)calloc(msg->message.lenMessage,sizeof(uint8_t));
        memcpy(MSG,msg->message.body.normal.payload,msg->message.lenMessage);

    }

    return MSG;
    
}


void Lowpan::cleanTimeoutMessage()
{
    if(!(bufferMsg.empty()))
    {   
        auto it = bufferMsg.begin();     
        while(it != bufferMsg.end())
        {
            auto msg = it->begin();
            
            while (msg != it->end())
            {
                msg->timeout--;
                if(msg->timeout == 0)
                {
                    Serial.println("---------------MESSAGGIO INCOMPLETO------------------");
                    free(msg->msg);

                    msg = it->erase(msg);
                }
                else
                {
                    msg++;
                }        
            }
        
            if(it->empty())
                it = bufferMsg.erase(it);
            else
            {
                it++;
            }    
        }
    }
}



