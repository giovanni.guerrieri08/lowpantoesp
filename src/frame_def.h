#ifndef FRAME_DEF_H
#define FRAME_DEF_H

#include "Arduino.h"

#define INIT_DATA_FCF 0b1010100001100001

#define TYPE_BEACON 0b000
#define TYPE_DATA 0b001
#define TYPE_ACK 0b010
#define TYPE_CMD 0b011

#define ADDR_NOPANID 0b00
#define ADDR_16BIT 0b10
#define ADDR_64BIT 0b11

#define BROADCAST_ADDRESS 0xffff


typedef union 
{ 
    uint16_t framecontrol;

    struct
    {
        uint16_t FrameType          :3;
        uint16_t SecurityEnabled    :1;
        uint16_t FramePending       :1;
        uint16_t AckRequest         :1;
        uint16_t PanIdCompression   :1;
        uint16_t                    :1;
        uint16_t sequenceNumSuppr   :1;
        uint16_t IeField            :1;    
        uint16_t DestAddrMode       :2;
        uint16_t FrameVersion       :2;
        uint16_t SrcAddrMode        :2;
    };

}FrameControl;


typedef union
{

    struct data
    {
        FrameControl fcf;
        uint8_t num_seq;
        uint16_t dest_panid;
        uint16_t dest_addr;
        uint16_t src_addr;
        uint8_t payload[116];
        uint16_t fcs;

    }DataFrame;

    struct ack
    {
        FrameControl fcf;
        uint8_t num_seq;
        uint16_t fcs;

    }AckFrame;
    
}frame_t;


typedef struct 
{
    bool typeFrame;

    /*
        TYPE: 0: DATAFRAME
              1: ACKFRAME
    */
    uint8_t frame_length;
    frame_t frame;
    uint8_t lqi;
    uint8_t rssi;

}rx_frame;



#endif