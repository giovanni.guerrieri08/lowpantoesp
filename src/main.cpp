// Define the type of device to be used: either ESP32 or ESP8266

#define ESP32
//#define ESP8266

#define GATEWAY (1)

#include <Arduino.h>

#include "uart/uart.h"
#include "handlePAN.h"

// Specify the radio channel 
// (according to IEEE 802.15.4: select channel number between 11..26)
#define CHANNEL               (12) 

// Specify the PAN ID (a 16-bit hex integer value)
#define PAN_ID                (0xB205)

// Specify the baudrate of the serial port for debugging purposes
#define BAUD_RATE             (115200) 

/*function to simulate the temperature sensor*/
uint8_t ReadSensorData()
{
   return rand() % (30 + 1 - 20) + 20;
}

/*defines the network object that records a callback sensor application function*/
handlePAN net(&ReadSensorData);

Uart uart(&Serial);



void setup()
{
        
   Serial.begin( BAUD_RATE );
   delay(1000);
   
   Serial.flush();


   #ifdef GATEWAY
   net.init(PAN_ID,CHANNEL,GATEWAY);
   
   #else
   net.init(PAN_ID,CHANNEL,0);
   #endif

   
   attachInterrupt(digitalPinToInterrupt(INT_PIN), [](){net.NetInterrupt();}, FALLING );
   interrupts();

   
}

void loop()
{

    if(uart.AvailableData())
    {     
         net.Send(uart.rxData,uart.length);
    }

    
    if(net.Receive())
    {

      uart.senData((uint8_t*)net.txData,net.lentxData);
      
    }
  
}








