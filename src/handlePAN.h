#ifndef __HANDLE_MSG_H__
#define __HANDLE_MSG_H__

#include "node.h"

#define LISTDEVICE 0x01


typedef struct
{
    uint16_t  addr;
    uint16_t value;

}sensor_t;



class handlePAN : public NODE
{
    
    public:

        typedef uint8_t (*sensorFunction)(void);

        handlePAN(sensorFunction function) : NODE() {

            _SensorFunction = function;
        }

        ~handlePAN();
        
        void handleGwMSG();
        
        bool Receive();

        uint16_t txData[1024];
        uint16_t lentxData;

    private:
        void SendDeviceList();
        void SendMessageList();

        vector <sensor_t> messages;
       
        sensorFunction _SensorFunction;
};



handlePAN::~handlePAN() {}


void handlePAN::handleGwMSG()
{
    switch (rx_message.rx_msg[0])
    {
        case LISTDEVICE:
            Serial.println("il gw vuole la lista dei devices");
            SendDeviceList();
            break;
        
        case LISTMESSAGE:
            if (rx_message.srcAddr == addrGateway)
            {
                //send the messages
                Serial.println("il gateway vuole la lista dei messaggi!");
                SendMessageList();
            }
            else
            {
                uint16_t addr = rx_message.srcAddr;
                auto el = find_if(messages.begin(),messages.end(),[addr](const sensor_t &it){
                    return (addr == it.addr);
                });

                if(el != messages.end())
                {
                    el->value = rx_message.rx_msg[1];
                }
                else
                {
                    sensor_t temp;
                    temp.value = rx_message.rx_msg[1];
                    temp.addr = addr;

                    messages.push_back(temp);
                }
                
                Serial.println("Message List:");
                for (auto it = messages.begin(); it != messages.end(); it++)
                {
                    Serial.printf("address: %x\tvalue %dC°\n",it->addr,it->value);
                }
            }

            break;
        
        default:
            break;
    }

    free(rx_message.rx_msg);

}

void handlePAN::SendDeviceList()
{
    vector<vector<uint16_t>> table = NODE::net->getRoutingTable()->table;
        
    uint8_t * message = (uint8_t*)calloc(2+((table.size()+1)*4),sizeof(uint8_t));
    
    int i = 0;
    
    message[i++] = LISTDEVICE;
    message[i++] = 0;
    
    memcpy(&message[i],(uint8_t*)&address,sizeof(uint16_t));
    i+=sizeof(uint16_t);

    message[i++] = 0;
    message[i++] = 0;

    for (auto it = table.begin(); it != table.end(); it++)
    {
        memcpy(&message[i],(uint8_t*)&(*it)[0],sizeof(uint16_t));
        i+=sizeof(uint16_t);

        memcpy(&message[i],(uint8_t*)&(*it)[2],sizeof(uint16_t));
        i+=sizeof(uint16_t);
    }
    
    NODE::SendToPan(message,i,addrGateway,true);


}

void handlePAN::SendMessageList()
{
    uint8_t * message = (uint8_t*)calloc(2+((messages.size()+1)*4),sizeof(uint8_t));

    int i = 0;
    message[i++] = LISTMESSAGE;
    message[i++] = 0;

    message[i++] = net->getMac()->address16_read();
    message[i++] = 0;
    
    /*value sensor of the lead node*/
    message[i++] = (*_SensorFunction)();
    message[i++] = 0;

    for (auto it = messages.begin(); it != messages.end(); it++)
    {
        memcpy(&message[i],(uint8_t*)&it->addr,sizeof(uint16_t));
        i+=sizeof(uint16_t);

        memcpy(&message[i],(uint8_t*)&it->value,sizeof(uint16_t));
        i+=sizeof(uint16_t);
    }
    
    NODE::SendToPan(message,i,addrGateway,true);

}

bool handlePAN::Receive()
{
    if(NODE::Receive(_SensorFunction))
    {  
        if(role == gateway)
        {
            memcpy(txData,(uint16_t*)rx_message.rx_msg,rx_message.len);

            lentxData = rx_message.len;

            free(rx_message.rx_msg);
            
            return true;
        }
        
        handleGwMSG();
        
    }

    return false;
}



#endif